<?php

/**
 * @file
 * Contains Drupal\Tests\cors\Unit\EventSubscriber\CorsResponseEventSubscriberTest.
 */

namespace Drupal\Tests\cors\Unit\EventSubscriber;

use Drupal\path_alias\AliasManagerInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\cors\EventSubscriber\CorsResponseEventSubscriber;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Drupal\Core\Path\PathMatcherInterface;

/**
 *
 */
class CorsResponseEventSubscriberTest extends UnitTestCase {

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => 'CORS Response Event Subscriber',
      'description' => 'Tests the CORS response event subscriber',
      'group' => 'CORS',
    );
  }

  /**
   * Tests adding CORS headers to the response.
   */
  public function testAddCorsHeaders() {
    $config_factory = $this->getConfigFactoryStub([
      'cors.settings' => [
        'domains' => ['* | http://example.com']
      ]
    ]);

    $alias_manager = $this->createMock(AliasManagerInterface::class);
    $path_matcher = $this->createMock(PathMatcherInterface::class);

    $path_matcher->expects(self::any())
      ->method('matchPath')
      ->withAnyParameters()
      ->will(self::returnValue(TRUE));

    // Create the response event subscriber.
    $subscriber = new CorsResponseEventSubscriber($config_factory, $alias_manager, $path_matcher);

    // Create the response event.
    $http_kernel = $this->createMock(HttpKernelInterface::class);
    $request = new Request();
    $response = new Response();
    $event = new ResponseEvent($http_kernel, $request, HttpKernelInterface::MAIN_REQUEST, $response);

    // Call the event handler.
    $subscriber->addCorsHeaders($event);

    self::assertEquals(
      '*',
      $response->headers->get('access-control-allow-origin'),
      'The access-control-allow-origin header was set'
    );
  }

}
